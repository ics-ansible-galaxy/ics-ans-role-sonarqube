import testinfra.utils.ansible_runner
import json
import os
import pytest
import time

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture(scope="module", autouse=True)
def setup(host):
    for i in range(200):
        cmd = host.command('curl -k https://ics-ans-role-sonarqube-default/api/system/status')
        status = json.loads(cmd.stdout)
        if status['status'] == "UP":
            return
        time.sleep(3)
    raise RuntimeError('Timed out waiting for application to start.')


def test_sonarqube_containers(host):
    with host.sudo():
        cmd = host.command("set +x; docker ps --format {{.Names}}")
    names = cmd.stdout.splitlines()
    assert all(name in names for name in [u'sonarqube', u'sonarqube_database', u'traefik_proxy'])


def test_sonarqube_application(host):
    cmd = host.command('curl -k https://ics-ans-role-sonarqube-default/')
    assert '<title>SonarQube</title>' in cmd.stdout


def test_sonarqube_plugin(host):
    ansible_plugin = host.file("/var/sonarqube/extensions/plugins/sonar-ansible-plugin-2.5.0.jar")
    assert ansible_plugin.exists
